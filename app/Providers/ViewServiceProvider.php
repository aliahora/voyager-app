<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Post;
use App\Category;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        View::composer('*', function ($view) {
            $view->with([
                'recentPosts' => $this->getRecentPosts(),
                'categories' => $this->getCategories()
            ]);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function getCategories()
    {
        return Category::orderBy('order','asc')->get();
    }

    public function getRecentPosts()
    {
        return Post::orderBy('id', 'desc')->take(5)->get();
    }
}
