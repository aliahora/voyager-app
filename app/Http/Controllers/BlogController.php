<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use OpenGraph;
use Illuminate\Support\Facades\Redirect;
use SEOMeta;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    public function getHome()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(10);

        $title = 'مجله';
        $description = 'مطالب بازیابی اطلاعات و تعمیر هارد';
        $keywords = [
            'مقاله',
            'مطلب',
            'blog',
            'اخبار',
            'اخبار بازیابی اطلاعات',
        ];


        SEOMeta::setTitle($title . ($posts->currentPage() > 1 ? ' - صفحه ' . $posts->currentPage() : ''));
        SEOMeta::setDescription($description);
        SEOMeta::setKeywords($keywords);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addProperty('locale', 'fa_IR');
        OpenGraph::addProperty('site_name', $title);
        OpenGraph::addImage(asset('images/logo/logo.png'));

        if (count($posts) == 0)  {
            return view('blog.empty', [
                'title' => $title,
                'message' => 'مطلبی یافت نشد.'
            ]);
        }

        return view('blog.list', [
            'title' => $title,
            'posts' => $posts,
        ]);
    }

    public function getPost($slug)
    {
        $post = Post::where('slug', $slug)->first();
        if (!$post) {
            abort(404);
        }

        $title = $post->title;
        $description = $post->excerpt;
        $keywords = $post->meta_keywords;

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::setKeywords($keywords);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'article');
        OpenGraph::addProperty('locale', 'fa_IR');
        OpenGraph::addProperty('site_name', $title);
        OpenGraph::addImage(asset('storage/'). '/' . $post->image);

        $tags = explode(',', $post->meta_keywords);
        $comments = Comment::where(['status' => '1', 'post_id' => $post->id])->get();
        return view('blog.post', [
            'title' => $title,
            'post' => $post,
            'tags' => $tags,
            'comments' => $comments,
        ]);
    }

    public function postComment(Post $post, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'email|required',
            'name' => 'required|min:3|max:20',
            'text' => 'required|min:10|max:200',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#comments")->withErrors($validator->errors())->withInput();
        }

        $comment = new Comment([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'body' => $request->input('text'),
        ]);

//        $comment = new Comment();
//        $comment->name = $request->input('name');
//        $comment->email = $request->input('email');
//        $comment->body = $request->input('text');

        if ($post->comments()->save($comment)) {
            return Redirect::to(URL::previous() . "#comments")->with(['comment_saved' => 'نظر شما با موفقیت ارسال شد و پس از بررسی نمایش داده خواهد شد']);
        } else {
            return Redirect::to(URL::previous() . "#comments")->with(['saving_error' => 'خطایی در ذخیره نظر شما رخ داد']);
        }
    }

    public function getCategory($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if (!$category) {
            abort(404);
        }

        $posts = Post::where('category_id', $category->id)->orderBy('id','desc')->paginate(10);

        $title = $category->name;
        $description = $category->name;
        $keywords = $category->name;

        SEOMeta::setTitle($title . ($posts->currentPage() > 1 ? ' - صفحه ' . $posts->currentPage() : ''));
        SEOMeta::setDescription($description);
        SEOMeta::setKeywords($keywords);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'category');
        OpenGraph::addProperty('locale', 'fa_IR');
        OpenGraph::addProperty('site_name', $title);
        OpenGraph::addImage(asset('images/logo/logo.png'));

        if (count($posts) == 0)  {
            return view('blog.empty', [
                'title' => $title,
                'message' => 'در این موضوع مطلبی درج نشده است.'
            ]);
        }

        return view('blog.list', [
            'title' => $title,
            'posts' => $posts,
        ]);
    }

    public function getSearch(Request $request)
    {
        $query = $request->input('query');

        $title = $query;
        $description = $query;
        $keywords = $query;

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::setKeywords($keywords);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'category');
        OpenGraph::addProperty('locale', 'fa_IR');
        OpenGraph::addProperty('site_name', $title);
        OpenGraph::addImage(asset('images/logo/logo.png'));

        if (!$request->exists('query') || empty($query)) {
            return redirect()->route('blog');
        }

        $posts = Post::where('title', 'like', '%' . $query . '%')->paginate(10);

        if (count($posts) == 0)  {
            return view('blog.empty', [
                'title' => $title,
                'message' => 'مطلبی با کلید واژه شما یافت نشد.'
            ]);
        }

        $posts->appends(['query' => $query]);

        return view('blog.list', [
            'title' => $title,
            'posts' => $posts,
        ]);
    }
}
