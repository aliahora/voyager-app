<?php

namespace App\Http\Controllers;

use App\Message;
use App\Post;
use Illuminate\Http\Request;
use OpenGraph;
use SEOMeta;
use URL;
use Validator;

class WebsiteController extends Controller
{
    public function getHome()
    {
        //$title = 'بازیابی اطلاعات هارد | تخصصی ترین مرکز تعمیر و بازیابی اطلاعات هارد دیسک';
        $title = 'کانون بازیابی اطلاعات | تخصصی ترین مرکز تعمیر و بازیابی هارد';
        $description = 'بازیابی اطلاعات هارد دیسک؛ تعمیرات هارد دیسک , قوی ترین مرکز بازیابی اطلاعات هارد دیسک , جراحی هارد دیسک با بروز ترین تجهیزات جراحی , بازیابی و تعمیر انواع هارد دیسک (SAS, NAS ,RAID ,SCAI , SSD, HDD) بازیابی اطلاعات (SD, CF, Flash و حافظه های NAND),بازیابی اطلاعات هارد DVR';
        $keywords = [
            'بازیابی',
            'Recovery',
            'ریکاوری',
            'data recovery',
            'بازیابی اطلاعات',
            'hard disk recovery',
            'بازیابی اطلاعات هارد',
            'repair hard',
            'تعمیر هارد',
            'فروش قطعات',
            'فروش دستگاه',
        ];

        SEOMeta::setTitle($title);
        SEOMeta::setDescription($description);
        SEOMeta::setKeywords($keywords);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl(URL::current());
        OpenGraph::addProperty('type', 'website');
        OpenGraph::addProperty('locale', 'fa_IR');
        OpenGraph::addProperty('site_name', $title);
        OpenGraph::addImage(asset('images/logo/logo.png'));

        $posts = Post::orderBy('id', 'desc')->take(3)->get();
        return view('home', compact('posts'));
    }

    public function postMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'Fname' => 'required|min:3|max:20',
            'Lname' => 'required|min:3|max:20',
            'email' => 'email|required',
            'sub' => 'required',
            'message' => 'required|min:10|max:200',
        ]);

        if ($validator->fails()) {
            return response(json_encode($validator->errors()), 400)
                ->header('Content-Type', 'application/json');
        }

        $message = new Message([
            'name' => $request->input('Fname') . ' ' . $request->input('Lname'),
            'email' => $request->input('email'),
            'subject' => $request->input('sub'),
            'message' => $request->input('message'),
        ]);
        if ($message->save()) {
            return response(json_encode($validator->errors()), 200)
                ->header('Content-Type', 'application/json');
        } else {
            return response(json_encode($validator->errors()), 400)
                ->header('Content-Type', 'application/json');
        }
    }
}
