<?php

namespace App\Widgets;

use App\Comment;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class CommentDimmer extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Comment::count();
        $string = 'Comments';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'voyager-bubble',
            'title' => "{$count} {$string}",
            'text' => "You have {$count} {$string}. Click on button below to view all {$string}.",
            'button' => [
                'text' => "view all {$string}",
                'link' => route('voyager.comments.index'),
            ],
            'image' => asset('images/comment-dimmer.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->can('browse', new Comment());
    }
}
