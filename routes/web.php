<?php

use Illuminate\Support\Facades\Artisan;

//Generate Site map
Route::get('/sitemap', function(){

	Artisan::call('sitemap:generate');
	return 'sitemap.xml created succsessfuly!';
});

//Website
Route::get('/', [
    'uses' => 'WebsiteController@getHome',
    'as' => 'home'
]);

Route::post('/message', [
    'uses' => 'WebsiteController@postMessage',
    'as' => 'post.message'
]);

//Blog
Route::get('/blog', [
    'uses' => 'BlogController@getHome',
    'as' => 'blog'
]);

Route::get('/blog/post/{slug}', [
    'uses' => 'BlogController@getPost',
    'as' => 'show.post'
]);

Route::get('/blog/category/{slug}', [
    'uses' => 'BlogController@getCategory',
    'as' => 'show.category'
]);

Route::get('/blog/search', [
    'uses' => 'BlogController@getSearch',
    'as' => 'show.search'
]);

Route::post('/blog/post/{post}/comment', [
    'uses' => 'BlogController@postComment',
    'as' => 'post.comment'
]);


//Admin
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
