@foreach ($items as $menu_item)
    @if (count($menu_item->children) > 0)
        <li class="dropdown-holder"><a href="#">{{ $menu_item->title }}</a>
            <ul class="sub-menu">
                @foreach($menu_item->children as $child)
                    <li><a href="{{ $child->url }}" class="tran3s">{{ $child->title }}</a></li>
                @endforeach
            </ul>
        </li>
    @else
        <li><a href="{{ $menu_item->url }}">{{ $menu_item->title }}</a></li>
    @endif
@endforeach
