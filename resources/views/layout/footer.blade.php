<!--
    =====================================================
        Footer
    =====================================================
    -->
<footer>
    <div class="container">
        <a href="{{route('home')}}" class="logo"><img src="{{asset('images/logo/logo.png')}}" alt="Logo"></a>

        <ul>
            <li><a href="#" class="tran3s round-border"><i class="fab fa-telegram-plane" aria-hidden="true"></i></a>
            </li>
            <li><a href="#" class="tran3s round-border"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
        </ul>
        <p>تمامی حقوق برای کانون بازیابی اطلاعات محفوظ می باشد.</p>
        {{-- <p>طراحی و توسعه <a href="https://t.me/mrrobotic404">علی اکبرزاده</a></p> --}}
    </div>
</footer>




<!-- =============================================
    Loading Transition
============================================== -->
{{-- <div id="loader-wrapper">
    <div id="preloader_1">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div> --}}



<!-- Scroll Top Button -->
<button class="scroll-top tran3s p-color-bg">
    <i class="fas fa-long-arrow-alt-up" aria-hidden="true"></i>
</button>




<!-- Js File_________________________________ -->

<!-- j Query -->
<script type="text/javascript" src="{{ asset('vendor/jquery.2.2.3.min.js') }}"></script>

<!-- Bootstrap JS -->
<script type="text/javascript" src="{{ asset('vendor/bootstrap/bootstrap.min.js') }}"></script>

<!-- Vendor js _________ -->

<!-- owl.carousel -->
<script type="text/javascript" src="{{ asset('vendor/owl-carousel/owl.carousel.min.js') }}"></script>
<!-- mixitUp -->
<script type="text/javascript" src="{{ asset('vendor/jquery.mixitup.min.js') }}"></script>
<!-- Progress Bar js -->
<script type="text/javascript" src="{{ asset('vendor/skills-master/jquery.skills.js') }}"></script>
<!-- Validation -->
<script type="text/javascript" src="{{ asset('vendor/contact-form/validate.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/contact-form/jquery.form.js') }}"></script>
<!-- Calendar js -->
<script type="text/javascript" src="{{ asset('vendor/monthly-master/js/monthly.js') }}"></script>


<!-- Theme js -->
<script type="text/javascript" src="{{ asset('js/persianumber.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/theme.js') . '?' . time() }}"></script>

</div> <!-- /.main-page-wrapper -->
</body>

</html>
