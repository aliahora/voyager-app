@include('layout.header')


<body>
<div id="app">
    <article class="blog-details-page">
        <div class="container">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 p-fix">
                @yield('content')
            </div> <!-- /.col- -->

            <!-- ========================== Aside Bar ======================== -->
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12 p-fix">
                <aside>
                    <form action="{{route('show.search')}}" class="sidebar-search-box" role="search">
                        <input name="query" dir="rtl" type="text" placeholder="جستجو..." required>
                        <button class="p-color-bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>

                    <div class="sidebar-news-list">
                        <h6>موضوعات</h6>
                        <ul>
                            @foreach($categories as $category)
                                <li>
                                    <a href="{{route('show.category',$category->slug)}}" class="tran3s">
                                        <i class="far fa-hand-point-left"></i>
                                        {{$category->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div> <!-- /.sidebar-news-list -->

                    <div class="sidebar-recent-post">
                        <h6>آخرین مطالب</h6>
                        @foreach($recentPosts as $post)
                            <div class="recent-single-post clear-fix">
                                <a href="{{route('show.post',$post->slug)}}"><img src="{{asset('storage/'). '/' . $post->image}}"
                                alt="Image"> </a>
                                {{--<img src="{{asset('storage/'). '/' . $post->image}}" alt="Image">--}}
                                <span class="post recentpostedit">
                                <a href="{{route('show.post',$post->slug)}}" class="tran3s">{{$post->title}}</a>

                                </span> <!-- /.post -->
                                <br/>
                                <span class="recentpostedit">{{\Morilog\Jalali\CalendarUtils::strftime('%d %B %Y', strtotime($post->created_at))}}</span>

                            </div> <!-- /.recent-single-post -->
                        @endforeach
                    </div>

                </aside>
            </div> <!-- /.col- -->
        </div> <!-- /.container -->
    </article>


</div>


@include('layout.footer')


