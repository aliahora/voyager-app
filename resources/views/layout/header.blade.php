<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}

    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="56x56" href="{{ asset('images/fav-icon/icon.png') }}">


    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:400,500,700">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/bootstrap.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/settings.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/layers.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/navigation.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.theme.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/WOW-master/css/libs/animate.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/skills-master/source/habilidades.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/hover.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/monthly-master/css/monthly.css') . '?' . time() }}">

    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') . '?' . time() }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') . '?' . time() }}">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script href="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script href="{{ asset('vendor/html5shiv.js') }}"></script>
    <script href="{{ asset('vendor/respond.js') }}"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
        crossorigin="anonymous">

    @include('layout.matomo')

</head>

<body>
    <div class="main-page-wrapper blog">



        <!--
    =============================================
        Theme Header
    ==============================================
    -->
        <header class="theme-main-header">
            <div class="container">
                <a href="{{route('home')}}" class="logo float-left tran4s"><img src="{{ asset('images/logo/logo.png') }}"
                        alt="Logo"></a>

                <!-- ========================= Theme Feature Page Menu ======================= -->
                <nav class="navbar float-right theme-main-menu one-page-menu">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1"
                            aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            منو
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">

                        <ul class="nav navbar-nav">

                            {{ menu('site','layout.menu') }}

                        </ul>

                    </div><!-- /.navbar-collapse -->
                </nav> <!-- /.theme-feature-menu -->
            </div>
        </header> <!-- /.theme-main-header -->

        <!--
    =====================================================
        Theme Inner page Banner
    =====================================================
    -->
        <section class="inner-page-banner">
            <div class="opacity">
                <div class="container">
                    <h2><a href="{{route('blog')}}">مجله</a></h2>
                    <ul>
                        <li><a href="{{route('home')}}">خانه</a></li>

                        <li> > </li>
                        <li>{{$title}}</li>
                        @if (isset($posts))
                            @if ($posts->currentPage() > 1)
                                <li> > </li>
                                <li> {{'صفحه ' . $posts->currentPage()}} </li>
                            @endif
                        @endif

                    </ul>
                </div> <!-- /.container -->
            </div> <!-- /.opacity -->
        </section> <!-- /.inner-page-banner -->


    </div>
