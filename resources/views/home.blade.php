<!DOCTYPE html>
<html lang="fa">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8">
    <meta http-equiv="Content-Language" content="fa" />
    <!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}

    <!-- For Resposive Device -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    {{--
    <link rel="icon" type="image/png" sizes="56x56" href="/images/fav-icon/icon.png">--}}
    <link rel="icon" type="image/png" href="{{ asset('images/fav-icon/icon.png') }}">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:400,500,700">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/bootstrap.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/settings.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/layers.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/revolution/navigation.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.carousel.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/owl-carousel/owl.theme.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/WOW-master/css/libs/animate.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/skills-master/source/habilidades.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/hover.css') . '?' . time() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/monthly-master/css/monthly.css') . '?' . time() }}">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>

    <!-- Main style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') . '?' . time() }}">
    <!-- responsive style sheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') . '?' . time() }}">


    <!-- Fix Internet Explorer ______________________________________-->

    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="vendor/html5shiv.js"></script>
    <script src="vendor/respond.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
        crossorigin="anonymous">

    @include('layout.matomo')

</head>

<body>
    <div class="main-page-wrapper">



        <!--
    =============================================
        Theme Header
    ==============================================
    -->
        <header class="theme-main-header">
            <div class="container">
                <a href="{{route('home')}}" class="logo float-left tran4s"><img src="images/logo/logo.png" alt="Logo"></a>

                <!-- ========================= Theme Feature Page Menu ======================= -->
                <nav class="navbar float-right theme-main-menu one-page-menu">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1"
                            aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            منو
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">

                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#home">خانه</a></li>
                            <li><a href="#service-section">خدمات</a></li>
                            <li><a href="#project-section">تصاویر</a></li>
                            <li><a href="#skill-section">تخصص</a></li>
                            <li class="dropdown-holder"><a href="#blog-section">مجله</a>
                                <ul class="sub-menu">
                                    <li><a href="{{route('blog')}}" class="tran3s">بیشتر</a></li>
                                </ul>
                            </li>
                            <li><a href="#team-section">تیم ما</a></li>
                            <li><a href="#contact-section">تماس با ما</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav> <!-- /.theme-feature-menu -->
            </div>
        </header> <!-- /...theme-main-header -->


        <!--
    =====================================================
        Theme Main SLider
    =====================================================
    -->
        <section id="home" class="banner">
            <div class="rev_slider_wrapper">
                <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
                <div id="main-banner-slider" class="rev_slider video-slider" data-version="5.0.7">
                    <ul>

                        <!-- SLIDE1  -->
                        <li data-index="rs-280" data-transition="fade" data-slotamount="default" data-easein="default"
                            data-easeout="default" data-masterspeed="default" data-title="Title Goes Here"
                            data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="images/home/slide-1.jpg" alt="image" class="rev-slidebg" data-bgparallax="3"
                                data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone"
                                data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0"
                                data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['-58','-58','0','-50']"
                                data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                                <h1>به کانون بازیابی اطلاعات خوش امدید</h1>
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['-05','-05','63','0']"
                                data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="2000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                                <h6>با بهترین خدمات برای شما</h6>

                            </div>



                        </li>


                        <!-- SLIDE2  -->
                        <li data-index="rs-280" data-transition="fade" data-slotamount="default" data-easein="default"
                            data-easeout="default" data-masterspeed="default" data-title="Title Goes Here"
                            data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="images/home/slide-1.jpg" alt="image" class="rev-slidebg" data-bgparallax="3"
                                data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone"
                                data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0"
                                data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['-58','-58','0','-50']"
                                data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                                <h2>به کانون بازیابی اطلاعات خوش امدید</h2>
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['-05','-05','63','0']"
                                data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="2000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                                <h6>با بهترین خدمات برای شما</h6>

                            </div>



                        </li>

                        <!-- SLIDE3  -->
                        <li data-index="rs-18" data-transition="fade" data-slotamount="default" data-easein="default"
                            data-easeout="default" data-masterspeed="default" data-thumb="images/home/slide-2.jpg"
                            data-rotate="0" data-saveperformance="off" data-title="Title Goes Here" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="images/home/slide-2.jpg" alt="image" class="rev-slidebg" data-bgparallax="3"
                                data-bgposition="center center" data-duration="20000" data-ease="Linear.easeNone"
                                data-kenburns="on" data-no-retina="" data-offsetend="0 0" data-offsetstart="0 0"
                                data-rotateend="0" data-rotatestart="0" data-scaleend="100" data-scalestart="140">
                            <!-- LAYERS -->

                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['-58','-33','-33','-100']"
                                data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="1000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                                <h2>به کانون بازیابی اطلاعات خوش امدید</h2>
                            </div>

                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['-05','93','93','20']"
                                data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="2000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; white-space: nowrap;">
                                <h6>با بهترین خدمات برای شما</h6>
                            </div>


                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                data-y="['middle','middle','middle','middle']" data-voffset="['52','185','185','105']"
                                data-transform_idle="o:1;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                                data-mask_in="x:0px;y:[100%];" data-mask_out="x:inherit;y:inherit;" data-start="3000"
                                data-splitin="none" data-splitout="none" data-responsive_offset="on">

                            </div>
                        </li>
                    </ul>
                </div>
            </div><!-- END REVOLUTION SLIDER -->
        </section> <!--  /#banner -->



        <!--
    =====================================================
        About us Section
    =====================================================
    -->
        <section id="about-us">
            <div class="container">
                <article class="theme-title">
                    <h2>چرا کانون بازیابی اطلاعات</h2>
                    <p>کانون بازیابی اطلاعات تخصصی ترین مرکز بازیابی اطلاعات انواع هارد دیسک های SSD , HDD بازیابی
                        اطلاعات سیستم های Raid می باشد . کانون بازیابی اطلاعات تنها مرکز تحقیقاتی در زمینه ی هارد دیسک
                        , به روز رسانی تکنولوژی های موجود در دنیا و پیاده سازی آنها با شرایط موجود می باشد .</p>
                </article> <!-- /.theme-title -->

                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fas fa-award"></i>
                            </div>
                            <h5><a href="#" class="tran3s">تخصص و ضمانت بازگشت اطلاعات</a></h5>

                        </div> <!-- /.single-about-content -->
                    </div> <!-- /.col -->

                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fas fa-hdd"></i>
                            </div>
                            <h5><a href="#" class="tran3s">بازیابی اطلاعات در کمترین زمان ممکن</a></h5>

                        </div> <!-- /.single-about-content -->
                    </div> <!-- /.col -->

                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fas fa-hand-holding-usd"></i>
                            </div>
                            <h5><a href="#" class="tran3s">پرداخت هزینه پس از تائید اطلاعات</a></h5>

                        </div> <!-- /.single-about-content -->
                    </div> <!-- /.col -->

                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="single-about-content">
                            <div class="icon round-border tran3s">
                                <i class="fas fa-compact-disc"></i>
                            </div>
                            <h5><a href="#" class="tran3s">پشتیبانی از اطلاعات بازیابی شده</a></h5>

                        </div> <!-- /.single-about-content -->
                    </div> <!-- /.col -->



                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /#about-us -->


        <!--
    =====================================================
        Service Section
    =====================================================
    -->
        <section id="service-section">
            <div class="container">
                <div class="theme-title">
                    <h2>خدمات</h2>
                </div> <!-- /.theme-title -->

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <section class="single-service-content">
                            <div class="icon-heading tran3s">
                                <div class="icon tran3s"><i class="fas fa-user-tie"></i></div>
                                <h6><a href="#" class="tran3s">تخصص </a></h6>
                            </div>
                            <p> مهندس نامدار در سال 2015 توانست پس از طی نمودن یک دوره ی فوق تخصصی در شرکت MRT نمایندگی
                                این شرکت را تا سال 2017 اخذ نماید و در سال 2017 نیز با برگزاری دوره فشرده و تخصصی در
                                شرکت Dolphin نمایندگی این دستگاه که یکی از کاملترین ابزار تعمیر و ریکاوری در دنیا می
                                باشد را نیز اخذ نماید </p>
                        </section> <!-- /.single-service-content -->
                    </div> <!-- /.col-lg -->

                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <section class="single-service-content">
                            <div class="icon-heading tran3s">
                                <div class="icon tran3s"><i class="fas fa-screwdriver"></i></div>
                                <h6><a href="#" class="tran3s">ابزار مدرن</a></h6>
                            </div>
                            <p>کانون بازیابی اطلاعات با بهره گیری از بروز ترین ابزار ریکاوری و تعمیر هارد دیسک به جرات
                                می توان کاملترین و تخصصی ترین مرکز در این زمینه نام برد <br />
                                استفاده از دستگاه ،raid Support،Dolphin PC3000Express
                            </p>
                        </section> <!-- /.single-service-content -->
                    </div> <!-- /.col-lg -->

                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <section class="single-service-content">
                            <div class="icon-heading tran3s">
                                <div class="icon tran3s"><i class="fas fa-business-time"></i></div>
                                <h6><a href="#" class="tran3s">سرعت کار</a></h6>
                            </div>
                            <p> اطلاعات فقط در زمان خاص و محدودی به درد ما می خورد و پس از سپری شدن زمان محدود ما حتی
                                اگر تمام اطلاعات بازیابی شود عملا دیگر بدرد ما نخواهد خورد . تعدد دستگاه های کانون
                                بازیابی اطلاعات باعث شده است که در بعضی از مواقع به علت فورس بودن زمان هارد در لحظه
                                ورود تعمیر و اطلاعات همان لحظه قابل رویت و بازیابی باشد</p>
                        </section> <!-- /.single-service-content -->
                    </div> <!-- /.col-lg -->

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /#service-section -->



        <!--
    =====================================================
        Project Section
    =====================================================
    -->
        <section id="project-section">
            <div class="container">
                <div class="theme-title">
                    <h2>تصاویر</h2>
                    <p>محیط کاری و ابزارهای فوق العاده ما</p>
                </div> <!-- /.theme-title -->

                <div class="project-menu">
                    <ul>
                        <li class="filter active tran3s" data-filter=".instruments">لابراتوری</li>
                        <li class="filter tran3s" data-filter=".dol">دلفین</li>
                        <li class="filter tran3s" data-filter=".mrt">MRT</li>
                    </ul>
                </div>

                <div class="project-gallery clear-fix">

                    <div class="mix grid-item instruments">
                        <div class="single-img">
                            <img src="images/project/abzar2.jpg" alt="Image">
                            {{-- <div class="opacity">
                                <div class="border-shape">
                                    <div>
                                        <div>
                                            <ul>
                                                <li>Business /</li>
                                                <li>Service /</li>
                                                <li>Product /</li>
                                                <li>Template</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> <!-- /.border-shape -->
                            </div> <!-- /.opacity --> --}}
                        </div> <!-- /.single-img -->
                    </div> <!-- /.grid-item -->

                    <div class="mix grid-item instruments">
                        <div class="single-img">
                            <img src="images/project/abzar3.jpg" alt="Image">
                        </div> <!-- /.single-img -->
                    </div> <!-- /.grid-item -->

                    <div class="mix grid-item instruments">
                        <div class="single-img">
                            <img src="images/project/abzar1.jpg" alt="Image" />
                        </div> <!-- /.single-img -->
                    </div> <!-- /.grid-item -->

                    <div class="mix grid-item instruments">
                        <div class="single-img">
                            <img src="images/project/abzar4.jpg" alt="Image">
                        </div> <!-- /.single-img -->
                    </div> <!-- /.grid-item -->

                    <div class="mix grid-item dol">
                        <div class="single-img">
                            <img src="images/project/dol1.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol2.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol3.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol4.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol5.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol6.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol7.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol8.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item dol">
                            <div class="single-img">
                                <img src="images/project/dol9.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt1.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt2.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt3.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt4.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt5.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt6.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt7.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt8.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt9.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                        <div class="mix grid-item mrt">
                            <div class="single-img">
                                <img src="images/project/mrt10.jpg" alt="Image">
                            </div> <!-- /.single-img -->
                        </div> <!-- /.grid-item -->

                    </div> <!-- /.project-gallery -->
                </div> <!-- /.container -->
        </section> <!-- /#project-section -->

        <!--
    =====================================================
        Page middle banner
    =====================================================
    -->

        <section class="page-middle-banner">
            <div class="opacity">
                <h3>ما مبتکر هستیم <span class="p-color">و</span> بهترین هارا ارائه میدهیم</h3>
            </div> <!-- /.opacity -->
        </section> <!-- /.page-middle-banner -->


        <!--
    =====================================================
        Team Section
    =====================================================
    -->
        <section id="team-section">
            <div class="container">
                <div class="theme-title">
                    <h2>با تیم ما اشنا شوید</h2>
                    <br />
                    <br />
                    <p>تخصص ما تضمین نتیجه شماست</p>

                </div> <!-- /.theme-title -->

                <div class="clear-fix team-member-wrapper">
                    <div class="float-left">
                        <div class="single-team-member">
                            <div class="img">
                                <img src="images/team/1.jpg" alt="Image">
                                <div class="opacity tran4s">
                                    <p>کارشناس ارشد بازیابی اطلاعات و تعمیرات انواع استوریج های اطلاعاتی و انواع هارد
                                        دیسک و انواع Ram در
                                        دوربین های Canon
                                    </p>
                                </div>
                            </div> <!-- /.img -->
                            <div class="member-name">
                                <p>محمد نامدار</p>
                                <p>کارشناس ارشد بازیابی</p>

                            </div> <!-- /.member-name -->
                        </div> <!-- /.single-team-member -->
                    </div> <!-- /float-left -->

                    <div class="float-left">
                        <div class="single-team-member">
                            <div class="img">
                                <img src="images/team/2.jpg" alt="Image">
                                <div class="opacity tran4s">

                                    <p>ایشان از جمله معدود متخصصین بازیابی اطلاعات از انواع هارد دیسک و سیستم های
                                        دوربین های مدار بسته
                                        حفاظتی هستند
                                    </p>
                                </div>
                            </div> <!-- /.img -->
                            <div class="member-name">
                                <p>امید قموشی</p>
                                <p>متخصص تعمیرات</p>

                            </div> <!-- /.member-name -->
                        </div> <!-- /.single-team-member -->
                    </div> <!-- /float-left -->

                    <div class="float-left">
                        <div class="single-team-member">
                            <div class="img">
                                <img src="images/team/3.jpg" alt="Image">
                                <div class="opacity tran4s">
                                    <p>ایشان با اشراف کامل به تمامی سخت افزار های کامپیوتر و نحوه تعمیرات و مسئول
                                        پیگیری کلیه قطعات , ورودی و امور مشتریان همیشه همراه واقعی مشتریان کانون
                                        بازیابی بوده و می باشند</p>
                                </div>
                            </div> <!-- /.img -->
                            <div class="member-name">
                                <p>خانم صمدی</p>
                                <p> امور حسابداری</p>
                            </div> <!-- /.member-name -->
                        </div> <!-- /.single-team-member -->
                    </div> <!-- /float-left -->
                </div> <!-- /.team-member-wrapper -->
            </div> <!-- /.conatiner -->
        </section> <!-- /#team-section -->



        <!--
    =====================================================
        Skill Section
    =====================================================
    -->
        <section id="skill-section">
            <div class="container">
                <div class="clear-fix">


                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="skills-progress skills">
                            <div class="habilidades_contenedor">
                                <div class="codeconSkills">
                                    <div class="codeconSkillbar">
                                        <div class="skill-text">
                                            <span class="codeconSkillArea">امنیت اطلاعات</span>
                                        </div>
                                        <div class="skillBar" data-percent="100%">
                                            <span class="PercentText">100%</span>
                                        </div>
                                    </div>
                                    <div class="codeconSkillbar">
                                        <div class="skill-text">
                                            <span class="codeconSkillArea ">رضایتمندی مشتری</span>

                                        </div>
                                        <div class="skillBar" data-percent="88%">
                                            <span class="PercentText">88%</span>
                                        </div>
                                    </div>
                                    <div class="codeconSkillbar">
                                        <div class="skill-text">
                                            <span class="codeconSkillArea">پشتکار</span>

                                        </div>
                                        <div class="skillBar" data-percent="85%">
                                            <span class="PercentText">85%</span>
                                        </div>
                                    </div>
                                    <div class="codeconSkillbar">
                                        <div class="skill-text">
                                            <span class="codeconSkillArea">تخصص</span>

                                        </div>
                                        <div class="skillBar" data-percent="80%">
                                            <span class="PercentText">80%</span>
                                        </div>
                                    </div>
                                    <div class="codeconSkillbar">
                                        <div class="skill-text">
                                            <span class="codeconSkillArea">مشتری مداری</span>

                                        </div>
                                        <div class="skillBar" data-percent="90%">
                                            <span class="PercentText ">90%</span>
                                        </div>
                                    </div>
                                </div> <!-- /.codeconSkills -->
                            </div> <!-- /.habilidades_contenedor -->
                        </div> <!-- /.skills-progress -->
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="img"><img src="images/home/2.jpg" alt="Image"></div>
                    </div> <!-- /.col- -->

                </div> <!-- /.clear-fix -->
            </div> <!-- /.container -->
        </section> <!-- /#skill-section -->



        <!--

    =====================================================
        Blog Section
    =====================================================
    -->
        <section id="blog-section">
            <div class="container">
                <div class="theme-title">
                    <h2>آخرین مطالب</h2>
                </div> <!-- /.theme-title -->
                <div class="clear-fix">
                    @foreach($posts as $post)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="single-news-item">
                            <div class="img"><img src="{{asset('storage/'). '/' . $post->image}}" alt="Image">
                                <a href="{{route('show.post',$post->slug)}}" class="opacity tran4s"><i class="fa fa-link"
                                        aria-hidden="true"></i></a>
                            </div> <!-- /.img -->

                            <div class="post">
                                <h6><a href="{{route('show.post',$post->slug)}}" class="tran3s">{{$post->title}}</a></h6>
                                منتشر شده توسط <span class="p-color">{{$post->user->name}}</span> در <b>{{\Morilog\Jalali\CalendarUtils::strftime('%d
                                    %B %Y', strtotime($post->created_at))}}</b>
                                <p>{{$post->excerpt}}...<a href="{{route('show.post',$post->slug)}}" class="tran3s">بیشتر</a></p>
                            </div> <!-- /.post -->
                        </div> <!-- /.single-news-item -->
                    </div> <!-- /.col- -->
                    @endforeach
                </div> <!-- /.clear-fix -->
            </div> <!-- /.container -->
        </section><!-- /#blog-section -->


        <!--
    =====================================================
        Partner Section
    =====================================================
    -->
        <section id="partner-section">
            <div class="container">
                <div class="theme-title">
                    <h2>برند های هارد</h2>
                </div> <!-- /.theme-title -->
                <br />
                <br />
                <br />
                <div id="partner_logo" class="owl-carousel owl-theme">
                    <div class="item"><img src="images/logo/1.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/2.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/3.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/4.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/5.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/6.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/7.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/8.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/9.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/10.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/11.jpg" alt="logo"></div>
                    <div class="item"><img src="images/logo/12.jpg" alt="logo"></div>
                </div> <!-- End .partner_logo -->
            </div> <!-- /.container -->
        </section> <!-- /#partner-section -->



        <!--
    =====================================================
        Contact Section
    =====================================================
    -->
        <section id="contact-section">
            <div class="container">
                <div class="clear-fix contact-address-content">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="left-side">
                            <h2>اطلاعات تماس</h2>

                            <ul>
                                <li>
                                    <div class="icon tran3s round-border p-color-bg"><i class="fas fa-map-marker-alt"
                                            aria-hidden="true"></i></div>
                                    <h6>آدرس</h6>
                                    <p>تهران , چهار راه ولیعصر , مجتمع تجاری ابریشم , طبقه 4 , واحد 405</p>
                                </li>
                                <li>
                                    <div class="icon tran3s round-border p-color-bg"><i class="fas fa-phone"
                                            aria-hidden="true"></i></div>
                                    <h6>تلفن</h6>
                                    <p>66479580 – 66479584 – 66479576 – 021</p>
                                </li>
                                <li>
                                    <div class="icon tran3s round-border p-color-bg"><i class="fas fa-envelope"
                                            aria-hidden="true"></i></div>
                                    <h6>آدرس الکترونیکی</h6>
                                    <p>info@bazyabi.com </p>
                                </li>
                            </ul>
                        </div> <!-- /.left-side -->
                    </div> <!-- /.col- -->


                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="map-area">
                            <h2>مکان ما</h2>
                            <div id="mapid"></div>
                        </div> <!-- /.map-area -->
                    </div> <!-- /.col- -->
                </div> <!-- /.contact-address-content -->



                <!-- Contact Form -->
                <div class="send-message">
                    <h2>ارسال پیام</h2>

                    <form action="{{route('post.message')}}" class="form-validation" autocomplete="off" method="post">
                        @csrf
                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="single-input">
                                    <input type="email" placeholder="آدرس الکترونیکی*" name="email">
                                </div> <!-- /.single-input -->
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="single-input">
                                    <input type="text" placeholder="نام خانوادگی*" name="Lname">
                                </div> <!-- /.single-input -->
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="single-input">
                                    <input type="text" placeholder="نام*" name="Fname">
                                </div> <!-- /.single-input -->
                            </div>

                        </div> <!-- /.row -->
                        <div class="single-input">
                            <input type="text" placeholder="موضوع" name="sub">
                        </div> <!-- /.single-input -->
                        <textarea placeholder="پیام خود را بنویسید" name="message"></textarea>


                        <button class="tran3s p-color-bg">ارسال</button>
                    </form>


                    <!-- Contact Form Validation Markup -->
                    <!-- Contact alert -->
                    <div class="alert-wrapper" id="alert-success">
                        <div id="success">
                            <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                            <div class="wrapper">
                                <p>پیام شما با موفقیت ارسال شد.</p>
                            </div>
                        </div>
                    </div> <!-- End of .alert_wrapper -->
                    <div class="alert-wrapper" id="alert-error">
                        <div id="error">
                            <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
                            <div class="wrapper">
                                <p>با عرض پوزش پیام شما ارسال نشد</p>
                            </div>
                        </div>
                    </div> <!-- End of .alert_wrapper -->
                </div> <!-- /.send-message -->
            </div> <!-- /.container -->
        </section> <!-- /#contact-section -->



        <!--
    =====================================================
        Footer
    =====================================================
    -->
        <footer>
            <div class="container">
                <a href="{{route('home')}}" class="logo"><img src="images/logo/logo.png" alt="Logo"></a>

                <ul>
                    <li><a href="#" class="tran3s round-border"><i class="fab fa-telegram-plane" aria-hidden="true"></i></a>
                    </li>
                    <li><a href="#" class="tran3s round-border"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
                <p>تمامی حقوق برای کانون بازیابی اطلاعات محفوظ می باشد</p>
                {{-- <p>طراحی و توسعه <a href="https://t.me/mrrobotic404">علی اکبرزاده</a></p> --}}
            </div>
        </footer>




        <!-- =============================================
        Loading Transition
    ============================================== -->
        {{-- <div id="loader-wrapper">
            <div id="preloader_1">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div> --}}


        <!-- Scroll Top Button -->
        <button class="scroll-top tran3s p-color-bg">
            <i class="fas fa-long-arrow-alt-up" aria-hidden="true"></i>
        </button>




        <!-- Js File_________________________________ -->

        <!-- j Query -->
        <script type="text/javascript" src="vendor/jquery.2.2.3.min.js"></script>

        <!-- Bootstrap JS -->
        <script type="text/javascript" src="vendor/bootstrap/bootstrap.min.js"></script>

        <!-- Vendor js _________ -->

        <!-- revolution -->
        <script type="text/javascript" src="vendor/revolution/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendor/revolution/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript" src="vendor/revolution/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendor/revolution/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendor/revolution/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendor/revolution/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendor/revolution/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendor/revolution/revolution.extension.video.min.js"></script>

        <!-- owl.carousel -->
        <script type="text/javascript" src="vendor/owl-carousel/owl.carousel.min.js"></script>
        <!-- mixitUp -->
        <script type="text/javascript" src="vendor/jquery.mixitup.min.js"></script>
        <!-- Progress Bar js -->
        <script type="text/javascript" src="vendor/skills-master/jquery.skills.js"></script>
        <!-- Validation -->
        <script type="text/javascript" src="vendor/contact-form/validate.js"></script>
        <script type="text/javascript" src="vendor/contact-form/jquery.form.js"></script>
        <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
   integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
   crossorigin=""></script>

        <!-- Theme js -->
        <script type="text/javascript" src="js/persianumber.min.js"></script>
        <script type="text/javascript" src="{{asset('js/theme.js') . '?' . time()}}"></script>
        <script type="text/javascript">
                // lat: 35.702402, lng: 51.405538
                var mymap = L.map('mapid').setView([35.702402, 51.405538], 16);
                L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                    attribution: '',
                    maxZoom: 18,
                    id: 'mapbox.streets',
                    accessToken: 'pk.eyJ1IjoiaGFsaXlhcmkiLCJhIjoiY2pyY2VkeTE0MDB1MTN5cnptejRtODRtYiJ9.SovtxOk0X96NEwEMPHDnAg'
                }).addTo(mymap);
                var marker = L.marker([35.702402, 51.405538]).addTo(mymap);
                marker.bindPopup("کانون بازیابی اطلاعات اینجاست!").openPopup();
        </script>

    </div> <!-- /.main-page-wrapper -->
</body>

</html>
