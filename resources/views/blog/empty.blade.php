@extends('layout.master')

@section('content')

<div class="jumbotron">
    <h6>{{$message}}</h6>
    <p>برای رفتن به صفحه مجله از کلید زیر استفاده کنید.</p>
    <p><a class="tran3s p-color-bg" href="{{ route('blog') }}" role="button">بازگشت</a></p>
  </div>

@endsection
