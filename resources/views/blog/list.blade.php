@extends('layout.master')

@section('content')

    @foreach($posts as $post)
        <div class="blog-details-post-wrapper ">
            <article class="hovereffect">
                <img class="img-responsive" src="{{asset('storage/'). '/' . $post->image}}"
                     alt="Image"/>
                <div class="overlay">

                    <p>
                        <a href="{{route('show.post',$post->slug)}}">بیشتر بخوانید</a>
                    </p>
                </div>
            </article>

            <div class="post-heading">
                <h4><a href="{{route('show.post',$post->slug)}}">{{$post->title}}</a></h4>
                <span> منتشر شده با موضوع <a class="tran3s p-color" href="{{ route('show.category',$post->category->slug) }}">{{$post->category->name}}</a> توسط <span class="tran3s p-color">{{$post->user->name}}</span> در <b>{{\Morilog\Jalali\CalendarUtils::strftime('%d
                    %B %Y', strtotime($post->created_at))}}</b></span>
                <br/>
                <br/>
                <div>{{ $post->excerpt }}...<a href="{{route('show.post',$post->slug)}}" type="button">بیشتر بخوانید</a></div>
            </div> <!-- /.post-heading -->
        </div>
    @endforeach

    {{ $posts->onEachSide(1)->links('layout.pagination') }}

@endsection
