@extends('layout.master')

@section('content')

<div class="blog-details-post-wrapper">

    <div class="hover14">
        <figure>
            <img src="{{asset('storage/'). '/' . $post->image}}" alt="Image">
        </figure>
    </div>
    <a></a>
    <div class="post-heading">
        <h4>{{$post->title}}</h4>
    <span> منتشر شده با موضوع <a class="tran3s p-color" href="{{ route('show.category',$post->category->slug) }}">{{$post->category->name}}</a> توسط <span class="tran3s p-color">{{$post->user->name}}</span> در <b>{{\Morilog\Jalali\CalendarUtils::strftime('%d
                %B %Y', strtotime($post->created_at))}}</b></span>
    </div> <!-- /.post-heading -->

    <br/>

    <div class="post-body">
        {!! $post->body !!}
    </div>

    <br/>
    <br/>

    <div class="clear-fix post-share-area">
        <ul class="share float-right">
            @foreach($tags as $tag)
            <li><a href="#" class="tran3s">{{$tag}},</a></li>
            @endforeach
        </ul>

    </div> <!-- /post-share-area -->


    <div id="comments" class="comment-area">
        <h4 dir="rtl">{{$comments->count()}} نظر</h4>

        <div class="comment-wrapper">
            @foreach($comments as $comment)
            <div class="single-comment clear-fix">
                <img src="https://api.adorable.io/avatars/64/{{$comment->email}}.png" alt="Image">
                <div class="comment float-left">
                    <h6>{{$comment->name}}</h6>
                    <i>{{\Morilog\Jalali\CalendarUtils::strftime('%d %B %Y', strtotime($post->created_at))}}</i>

                    <p>{{$comment->body}}</p>
                </div> <!-- /.comment -->
            </div> <!-- /.single-comment -->
            @endforeach
        </div> <!-- /.comment-wrapper -->
    </div> <!-- /.comment-area -->

    @if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach ($errors->all() as $e)
            <li>{{$e}}</li>
            @endforeach
        </ul>
    </div>
    @endif

    @if (session()->has('comment_saved'))
    <div class="alert alert-success" role="alert">
        {{session()->get('comment_saved')}}
    </div>

    @endif

    @if (session()->has('saving_error'))
    <div class="alert alert-danger" role="alert">
        {{session()->get('saving_error')}}
    </div>

    @endif



    <div class="post-comment">
        <h4>نظر خود را با ما به اشتراک بگذارید</h4>

        <form action="{{route('post.comment',$post)}}" method="post">
            @csrf
            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <span>آدرس الکترونیکی*</span>
                    <div class="single-input">
                    <input type="email" name="email" value="{{ old('email') }}" required>
                    </div> <!-- /.single-input -->
                </div> <!-- /.col- -->

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <span>نام*</span>
                    <div class="single-input">
                        <input type="text" name="name" value="{{ old('name') }}" required>
                    </div> <!-- /.single-input -->
                </div> <!-- /.col- -->

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span>نظر شما*</span>
                    <div class="single-input">
                        <textarea name="text" required>{{ old('text') }}</textarea>
                    </div> <!-- /.single-input -->
                </div> <!-- /.col- -->
            </div> <!-- /.row -->

            <button class="tran3s p-color-bg">ارسال نظر</button>
        </form>
    </div> <!-- /.post-comment -->
</div> <!-- /.blog-details-post-wrapper -->

@endsection
